﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    Player P;
    Rigidbody2D rb;
    public bool ground = true;
    private bool[] hk=new bool[2];//the currently held down keys 0=A, 1=D;
    public float accelerate;//between 0 and 1 indicates how fast u can accelerate;
    public float decay;//how fast ur speed is decaying;
    public float vel;//indicates how fast u moving right now;
    float checkradius = .6f;//groundcheck radius
    Vector3 checkCenter = new Vector3(0f, 0f, 0f);//groundcheck center point
    public LayerMask groundLayer = new LayerMask();
    public float ERegen;//Energy regeneration per second;
    private void Start()
    {
        P = GetComponent<Player>();
        rb = GetComponent<Rigidbody2D>();
    }

    //when A or D pressed
    public void Down(int dir)
    {
        //dir is -1 or 1
        hk[Math.Max(0, dir)] = true;//sets the keys;
        if (hk[1 - Math.Max(0, dir)])//this means theother key is being held down too;
        {
            //lets jump;
            Jump();
            dir = -dir;
        }

        
        float force = P.Energy * .03f;//how much can we accelerte?
        P.Energy -= force;
        float pot = dir * P.speed - vel;//how much can we accelerate until fullspeed?
        if (P.dir != Math.Sign(dir))
        {
            if (Math.Abs(vel) < Math.Abs(pot * accelerate * force))
            {
                vel = 0;
                setDir(dir);
            }
            else
            {
                vel += pot * accelerate * force;
            }
        }
        else
        {
            vel += pot * accelerate * force;
        }
    }

    
    public void Up(int dir)
    {
        hk[Math.Max(0, dir)] = false;//sets the keys;
    }
    private void FixedUpdate()
    {
        if (hk[0] || hk[1])
        {
            //keep vel as it is
        }
        else
        {
            vel = vel - Math.Sign(vel) * decay * Time.fixedDeltaTime;

        }
    }
    public void Update()
    {
        P.Energy =Math.Min(1000f,P.Energy+ ERegen * Time.deltaTime);
        checkGround();
        if (vel > 0.1f)
        {
            if (P.dir == -1)
            {
                setDir(1);
            }
        }
        else if(vel<-.1f)
        {
            if (P.dir == 1)
            {
                setDir(-1);
            }
        }
        transform.Translate(vel * Time.deltaTime, 0f, 0f);
        


    }

    private void Jump()
    {
        if (ground)
        {
            float force = P.Energy * .03f;
            P.Energy -= force;
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            rb.AddForce(Vector2.up * 15f*force);
        }
    }

    private void checkGround()
    {

        ground = Physics2D.OverlapCircleAll(checkCenter + transform.position, checkradius, groundLayer).Length != 0;

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + checkCenter, checkradius);
    }
    void setDir(int v)
    {
        setDir((float)v);
    }//sets the direction of player
    void setDir(float v)
    {
        P.dir = -1;
        if (v > 0)
        {
            P.dir = 1;
        }
        transform.localScale = new Vector3(P.dir * Math.Abs(transform.localScale.x), transform.localScale.y, 1f);
    }
}
