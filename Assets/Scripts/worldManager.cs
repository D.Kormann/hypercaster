﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class worldManager : MonoBehaviour
{
    public Transform RightEnd;
    public Transform LeftEnd;
    public int currentLevel;
    Scene[] scenes=new Scene[3];


    private void Start()
    {
        scenes[0] = SceneManager.GetSceneByBuildIndex(1);
        scenes[1] = SceneManager.GetSceneByBuildIndex(2);
        scenes[2] = SceneManager.GetSceneByBuildIndex(3);

        SceneManager.LoadSceneAsync("scene 1");
       // SceneManager.LoadSceneAsync()
        SceneManager.LoadSceneAsync(2);
        SceneManager.LoadSceneAsync(3);
        //SceneManager.SetActiveScene(scenes[0]);
        SceneManager.SetActiveScene(scenes[0]);
    }

    public void nextLevel(int v)//v can be -1 or 1 depending on where the player wants to go
    {
        Time.timeScale = 0f;
        int nextLevel = 3 % (currentLevel + v);
        
        SceneManager.SetActiveScene(scenes[nextLevel]);
    }
}
