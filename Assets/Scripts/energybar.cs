﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class energybar : MonoBehaviour
{
    public Transform EBar;
    public Player P;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        EBar.localScale = new Vector3(P.Energy / 1000f, 1f, 1f);
        EBar.localPosition = new Vector3(-.5f+P.Energy / 2000f, 0f, 0f);
    }
}
