﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    static protected Transform[] Missile;
    public float speed;
    public float health;
    static Action<Figure>[] book =new Action<Figure>[256];
    // Start is called before the first frame update
    void Start()
    {
        setUpBook();
    }

    
   
    
    protected void Fire(int color, int size)
    {
        if (!this is Player)
        {
            print("cannot jump!");
            return;
        }
        Player me = (Player)this;

        Missile m = Instantiate(Missile[color], transform.position, Quaternion.identity).GetComponent<Missile>();
        m.init(size,me.dir);
       
    }

    static protected void setUpBook()
    {
        book[ 0] = (dis) => dis.Fire(0, 1);
        book[ 1] = (dis) => dis.Fire(1, 1);
        book[ 2] = (dis) => dis.Fire(2, 1);
        book[ 3] = (dis) => dis.Fire(0, 2);
        book[ 4] = (dis) => dis.Fire(1, 2);
        book[ 5] = (dis) => dis.Fire(2, 2);
        book[ 6] = (dis) => dis.Fire(0, 3);
        book[ 7] = (dis) => dis.Fire(1, 3);
        book[ 8] = (dis) => dis.Fire(2, 3);
        book[ 9] = (dis) => dis.Fire(0, 4);
        book[10] = (dis) => dis.Fire(1, 4);
        book[11] = (dis) => dis.Fire(2, 4);
        book[12] = (dis) => dis.Fire(0, 5);
        book[13] = (dis) => dis.Fire(1, 5);
        book[14] = (dis) => dis.Fire(2, 5);
        book[15] = (dis) => dis.Fire(0, 6);
        book[16] = (dis) => dis.Fire(1, 6);


    }

    protected void Spell(byte sequence)
    {
        book[sequence-2](this);
        
    }
}
