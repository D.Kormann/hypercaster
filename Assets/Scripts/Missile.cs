﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : MonoBehaviour
{
    float time = 2f;
    public float speed;
    int dir;
    public void init(int size,int dir)
    {
        transform.localScale = new Vector3(transform.localScale.x*dir*size,transform.localScale.x*size,1);
        this.dir = dir;
        time += Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime*dir, 0f, 0f);
        if (Time.time > time)
        {
            Destroy(gameObject);
        }
    }
}
